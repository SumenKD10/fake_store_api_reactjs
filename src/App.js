import "./App.css";
import React, { Component } from "react";
import ShopPage from "./components/ShopPage.js";

class App extends Component {
  render() {
    return (
      <div>
        <ShopPage />
      </div>
    );
  }
}

export default App;
