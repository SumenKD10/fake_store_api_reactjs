import React, { Component } from "react";

class Navbar extends Component {
  render() {
    return (
      <nav>
        <h1>Do-Shop</h1>
        <ul className="menuList">
          <li>Home</li>
          <li>Offers</li>
          <li>Support</li>
          <li>Careers</li>
        </ul>
        <div>
          <input type="button" value="Login" className="navButton"></input>
          <input type="button" value="Sign Up" className="navButton"></input>
        </div>
      </nav>
    );
  }
}

export default Navbar;
