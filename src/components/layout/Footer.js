import React, { Component } from "react";

class Footer extends Component {
  render() {
    return (
      <footer>
        <h1>Do-Shop</h1>
        <p>All rights reserved © Sumen Kumar Dubey</p>
        <h6>Contacts: 7979XXX7##</h6>
      </footer>
    );
  }
}

export default Footer;
