import React, { Component } from "react";
import Navbar from "./layout/Navbar.js";
import ItemContainer from "./ItemContainer.js";
import Footer from "./layout/Footer.js";

class ShopPage extends Component {
  constructor(props) {
    super(props);

    this.API_STATUS = {
      LOADING: "loading",
      LOADED: "loaded",
      ERROR: "error",
    };

    this.state = {
      itemList: [],
      status: this.API_STATUS.LOADING,
    };
    this.API_URL = "https://fakestoreapi.com/products";
  }

  render() {
    return (
      <>
        <Navbar />
        {this.state.status === this.API_STATUS.LOADING && (
          <div className="lds-roller">
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>{" "}
          </div>
        )}
        {this.state.status === this.API_STATUS.ERROR && (
          <h1 className="message">Internal Server Error!</h1>
        )}
        {this.state.status === this.API_STATUS.LOADED &&
          this.state.itemList.length === 0 && (
            <h1 className="message">No Products Available!</h1>
          )}
        {this.state.status === this.API_STATUS.LOADED &&
          this.state.itemList.length !== 0 && (
            <ItemContainer allItems={this.state.itemList} />
          )}
        <Footer />
      </>
    );
  }

  componentDidMount() {
    this.setState({ status: this.API_STATUS.LOADING });
    fetch(this.API_URL)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        this.setState({ itemList: data, status: this.API_STATUS.LOADED });
      })
      .catch((errorMessage) => {
        console.error(errorMessage);
        this.setState({ status: this.API_STATUS.ERROR });
      });
  }
}

export default ShopPage;
