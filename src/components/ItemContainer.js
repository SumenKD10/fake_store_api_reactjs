import React, { Component } from "react";
import EachItem from "./EachItem";

class ItemContainer extends Component {
  render() {
    return (
      <div className="itemContainer">
        {this.props.allItems.map((eachItem) => {
          return <EachItem key={eachItem.id} itemProperties={eachItem} />;
        })}
      </div>
    );
  }
}

export default ItemContainer;
