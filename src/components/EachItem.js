/* eslint-disable jsx-a11y/img-redundant-alt */
import React, { Component } from "react";

class EachItem extends Component {
  render() {
    return (
      <div className="eachItemCard">
        <p>📍{this.props.itemProperties.category}</p>
        <img
          src={this.props.itemProperties.image}
          alt="No Image Available"
          className="itemImage"
        />
        <div className="ratingDiv">
          <p>⭐{this.props.itemProperties.rating.rate}</p>
          <p>👤{this.props.itemProperties.rating.count}</p>
        </div>
        <div>
          <h4 className="itemTitle">{this.props.itemProperties.title}</h4>
        </div>
        <p className="price">${this.props.itemProperties.price}</p>
        <input
          type="submit"
          value="Add to Cart"
          className="addCartButton"
        ></input>
      </div>
    );
  }
}

export default EachItem;
